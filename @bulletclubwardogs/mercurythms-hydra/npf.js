addScripts([
  "https://static.tumblr.com/gtjt4bo/sgMrymui0/waitforelement.js",
]).then(() => {
  waitForElement("[post-type='text'] .npf-no-caption", { end: 2000 }).then(() => {
    document.querySelectorAll("[post-type='text'] .npf-no-caption")?.forEach(e => {
      e.closest(".text-caption").classList.add("tbd");
      e.closest(".text-caption").before(e);
      
      if(e.nextElementSibling.matches(".tbd")){
        e.nextElementSibling.remove()
      }
      
      e.style.marginBottom = "var(--NPF-Caption-Spacing)"
    })
  }).catch(err => {
    console.error(err)
  })
}).catch(err => {
  console.error(err);
});
